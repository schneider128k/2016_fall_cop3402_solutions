#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

int code_length;
char source[1000];

typedef enum {
  nulsym = 1, identsym, numbersym, plussym, minussym,
  multsym, slashsym, oddsym, eqsym, neqsym, lessym, leqsym,
  gtrsym, geqsym, lparentsym, rparentsym, commasym, semicolonsym,
  periodsym, becomessym, beginsym, endsym, ifsym, thensym,
  whilesym, dosym, callsym, constsym, varsym, procsym, writesym,
  readsym , elsesym
} token_type;


char reserved_word[14][11] = {
  "const",     "var",
  "procedure", "call",
  "begin",     "end",
  "if",        "then", "else",
  "while",     "do",
  "read",      "write",
  "odd"
};

token_type reserved_word_type[14] = {
  constsym,    varsym,
  procsym,     callsym,
  beginsym,    endsym,
  ifsym,       thensym, elsesym,
  whilesym,    dosym,
  readsym,     writesym,
  oddsym
};

char special_lexeme[16][3] = {
  ":=", "<=", ">=", "<>", "=", "<", ">", "(", ")", "+", "-", "*", "/", ",", ";", "."
};

token_type special_lexeme_type[16] = {
  becomessym, leqsym, geqsym, neqsym, eqsym, lessym, gtrsym, lparentsym, rparentsym, plussym, minussym, multsym, slashsym,
  commasym, semicolonsym, periodsym
};

// pl0_code_filename
char *pl0_code_filename;

// load code
void loadSourceProgram() {
  FILE *input = fopen( pl0_code_filename, "r" );

  if (!input) {
    printf("could not open pl0 filename %s\n", pl0_code_filename);
    exit(1);
  }

  int index = 0;

  char c;

  fscanf( input, "%c", &c );

  while ( !feof( input ) ) {
    // printf("pos: %2d, ASCII: %3d\n", index, c );
    source[ index ] = c;
  	index++;
    fscanf( input, "%c", &c );
  }
  // printf("\n");

  code_length = index;

  // printf("code length: %d\n", code_length );

  fclose( input );
}

void printSourceProgram() {
  printf("\nsource code:\n");
  printf("------------\n");
  printf("%s", source);
}

int hasSubstringAtPos(char *substring, int i) {
  int offset = 0;
  while ( ( substring[offset] != '\0' ) && (i + offset < code_length) && ( substring[offset] == source[i + offset] ) ) {
    offset++;
  }
  if ( substring[offset] != '\0' ) {
    return 0;
  }
  return 1;
}

int hasReservedWordAtPos(char *substring, int i) {
  int offset = 0;
  while ( ( substring[offset] != '\0' ) && (i + offset < code_length) && ( substring[offset] == source[i + offset] ) ) {
    offset++;
  }
  if ( substring[offset] != '\0' ) {
    return 0;
  }
  if ( isalnum( source[i + offset] ) ) {
    return 0;
  }
  return 1;
}

// printCleanSourceProgram
void printCleanSourceProgram() {
  // current position in source program
  int index = 0;
  // current character
  char ch;
  // flag indicating if we are inside a comment
  int is_inside_comment = 0;

  printf("\nsource code without comments:\n");
  printf("-----------------------------\n");

    // begin check
    begin_check:
    if ( index >= code_length ) {
      return; // we are done with printing
    }

    // inside comment
    if (is_inside_comment) {
      // end of comment
      if (hasSubstringAtPos("*/", index)) {
        is_inside_comment = 0;
        index = index + 2;
        printf("  ");
      }
      // still inside comment
      else {
        index++;
        printf(" ");
      }
      goto begin_check;
    }

    if (hasSubstringAtPos("/*", index)) {
      is_inside_comment = 1;
      index = index + 2;
      printf("  ");
      goto begin_check;
    }

    printf("%c", source[index]);
    index++;
    goto begin_check;
}

// lexer
void lexer() {
  // current position in source program
  int index = 0;
  // current character
  char ch;
  // flag indicating if we are inside a comment
  int is_inside_comment = 0;
  // int var used to loop over reserved words, operators, and symbols
  int w;
  // numsym
  int num;
  int num_len;
  // identsym
  char ident_name[11];
  int ident_name_len;

  printf("\ntokens:\n");
  printf("-------\n");

    // begin check
    begin_check:
    if ( index >= code_length ) {
      return; // we are done with the lexical analysis
    }

    // inside comment
    if (is_inside_comment) {
      // end of comment
      if (hasSubstringAtPos("*/", index)) {
        is_inside_comment = 0;
        index = index + 2;
      }
      // still inside comment
      else {
        index++;
      }
      goto begin_check;
    }

    // begin detect start of comment
    if (hasSubstringAtPos("/*", index)) {
      is_inside_comment = 1;
      index = index + 2;
      goto begin_check;
    }
    // end detect start of comment

    // begin detect reserved words
    for (w = 0; w < 14; w++) {
      //printf("checking reserved word: %d\n", w);
      if (hasReservedWordAtPos(reserved_word[w], index)) {
        index = index + strlen(reserved_word[w]);
        printf("%-12s %d\n", reserved_word[w], reserved_word_type[w]);
        goto begin_check;
      }
    }
    // end detect reserved words

    // begin detect special lexemes
    for (w = 0; w < 16; w++) {
      //printf("checking special lexeme: %d\n", w);
      if (hasSubstringAtPos(special_lexeme[w], index)) {
        index = index + strlen(special_lexeme[w]);
        printf("%-12s %d\n", special_lexeme[w], special_lexeme_type[w]);
        goto begin_check;
      }
    }
    // end detect special lexemes

    // begin detect numbers
    if ( isdigit( ch = source[index] ) ) {
      num = 0;
      num_len = 0;
      while ( (index < code_length) && isdigit( ch = source[index]) ) {
        num = 10 * num + (ch - '0');
        num_len++;
        index++;
        if ( num_len > 5) {
          printf("LEX ERROR: Number too long.\n");
          return;
        }
      }
      if ( isalpha( ch) ) {
        printf("LEX ERROR: Variable name does not start with a letter.");
        return;
      }
      printf("%-12d %d\n", num, numbersym);
      goto begin_check;
    }
    // end detect numbers

    // begin detect numbers
    if ( isalpha( ch = source[index] ) ) {
      ident_name[0] = ch;
      ident_name_len = 1;
      index++;
      while ( (index < code_length) && isalnum( ch = source[index]) ) {
        ident_name[ ident_name_len ] = ch;
        ident_name_len++;
        index++;
        if ( ident_name_len > 11 ) {
          printf("LEX ERROR: Variable name is too long\n");
          return;
        };
      }
      ident_name[ ident_name_len ] = '\0';
      printf("%-12s %d\n", ident_name, identsym);
      goto begin_check;
    }
    // end detect numbers

    // begin detect white space
    if ( isspace(ch = source[index]) ) {
      index++;
      goto begin_check;
    }
    // end detect white space

    printf("LEX ERROR: Illegal character.");
    return;
}

int main(int argc, const char* argv[]) {

  if (argc != 2) {
    printf("missing pl0 code filename\n");
    return 0;
  }

  pl0_code_filename = malloc(strlen(argv[1] + 1));
  strcpy(pl0_code_filename, argv[1]);

  loadSourceProgram();
  printSourceProgram();
  printCleanSourceProgram();
  lexer();
  return 0;
}

