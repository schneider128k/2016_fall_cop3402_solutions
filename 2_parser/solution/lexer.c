#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "token_type.h"

int code_length;
char source[1000];

char reserved_word[14][11] = {
  "const",     "var",
  "procedure", "call",
  "begin",     "end",
  "if",        "then", "else",
  "while",     "do",
  "read",      "write",
  "odd"
};

token_type reserved_word_type[14] = {
  constsym,    varsym,
  procsym,     callsym,
  beginsym,    endsym,
  ifsym,       thensym, elsesym,
  whilesym,    dosym,
  readsym,     writesym,
  oddsym
};

char special_lexeme[16][3] = {
  ":=", "<=", ">=", "<>", "=", "<", ">", "(", ")", "+", "-", "*", "/", ",", ";", "."
};

token_type special_lexeme_type[16] = {
  becomessym, leqsym, geqsym, neqsym, eqsym, lessym, gtrsym, lparentsym, rparentsym, plussym, minussym, multsym, slashsym,
  commasym, semicolonsym, periodsym
};

// load code
void loadSourceProgram() {
  FILE *input = fopen( "input.txt", "r" );

  int index = 0;

  char c;

  fscanf( input, "%c", &c );

  while ( !feof( input ) ) {
    source[ index ] = c;
  	index++;
    fscanf( input, "%c", &c );
  }
  code_length = index;
  fclose( input );
}

void printSourceProgram() {
  printf("\ninput.txt\n");
  printf("---------\n");
  printf("%s", source);
}

int hasSubstringAtPos(char *substring, int i) {
  int offset = 0;
  while ( ( substring[offset] != '\0' ) && (i + offset < code_length) && ( substring[offset] == source[i + offset] ) ) {
    offset++;
  }
  if ( substring[offset] != '\0' ) {
    return 0;
  }
  return 1;
}

int hasReservedWordAtPos(char *substring, int i) {
  int offset = 0;
  while ( ( substring[offset] != '\0' ) && (i + offset < code_length) && ( substring[offset] == source[i + offset] ) ) {
    offset++;
  }
  if ( substring[offset] != '\0' ) {
    return 0;
  }
  if ( isalnum( source[i + offset] ) ) {
    return 0;
  }
  return 1;
}

// printCleanSourceProgram
void printCleanSourceProgram() {
  // current position in source program
  int index = 0;
  // current character
  char ch;
  // flag indicating if we are inside a comment
  int is_inside_comment = 0;

  printf("\ncleaninput.txt\n");
  printf("--------------\n");

    // begin check
    begin_check:
    if ( index >= code_length ) {
      return; // we are done with printing
    }

    // inside comment
    if (is_inside_comment) {
      // end of comment
      if (hasSubstringAtPos("*/", index)) {
        is_inside_comment = 0;
        index = index + 2;
        printf("  ");
      }
      // still inside comment
      else {
        index++;
        printf(" ");
      }
      goto begin_check;
    }

    if (hasSubstringAtPos("/*", index)) {
      is_inside_comment = 1;
      index = index + 2;
      printf("  ");
      goto begin_check;
    }

    printf("%c", source[index]);
    index++;
    goto begin_check;
}

// lexer
void lexer() {
  // current position in source program
  int index = 0;
  // current character
  char ch;
  // flag indicating if we are inside a comment
  int is_inside_comment = 0;
  // int var used to loop over reserved words, operators, and symbols
  int w;
  // numsym
  int num;
  int num_len;
  // identsym
  char ident_name[11];
  int ident_name_len;

  // open file lexemelist.txt
  FILE *f = fopen("lexemetable.txt", "w"); // write mode
  if (f == NULL) {
    printf("Error opening lexemetable.txt\n");
    return;
  }

  printf("\nlexemetable.txt\n");
  printf("---------------\n");
  printf("lexeme       token type\n");

    // begin check
    begin_check:
    if ( index >= code_length ) {
      fclose(f);
      return; // we are done with the lexical analysis
    }

    // inside comment
    if (is_inside_comment) {
      // end of comment
      if (hasSubstringAtPos("*/", index)) {
        is_inside_comment = 0;
        index = index + 2;
      }
      // still inside comment
      else {
        index++;
      }
      goto begin_check;
    }

    // begin detect start of comment
    if (hasSubstringAtPos("/*", index)) {
      is_inside_comment = 1;
      index = index + 2;
      goto begin_check;
    }
    // end detect start of comment

    // begin detect reserved words
    for (w = 0; w < 14; w++) {
      //printf("checking reserved word: %d\n", w);
      if (hasReservedWordAtPos(reserved_word[w], index)) {
        index = index + strlen(reserved_word[w]);
        printf("%-12s %d\n", reserved_word[w], reserved_word_type[w]);
        // write to lexeme table
        fprintf(f, "%d ", reserved_word_type[w]);
        goto begin_check;
      }
    }
    // end detect reserved words

    // begin detect special lexemes
    for (w = 0; w < 16; w++) {
      //printf("checking special lexeme: %d\n", w);
      if (hasSubstringAtPos(special_lexeme[w], index)) {
        index = index + strlen(special_lexeme[w]);
        printf("%-12s %d\n", special_lexeme[w], special_lexeme_type[w]);
        fprintf(f, "%d ", special_lexeme_type[w]);
        goto begin_check;
      }
    }
    // end detect special lexemes

    // begin detect numbers
    if ( isdigit( ch = source[index] ) ) {
      num = 0;
      num_len = 0;
      while ( (index < code_length) && isdigit( ch = source[index]) ) {
        num = 10 * num + (ch - '0');
        num_len++;
        index++;
        if ( num_len > 5) {
          printf("LEX ERROR: Number too long.\n");
          fclose(f);
          return;
        }
      }
      if ( isalpha( ch) ) {
        printf("LEX ERROR: Variable name does not start with a letter.");
        fclose(f);
        return;
      }
      printf("%-12d %d\n", num, numbersym);
      fprintf(f, "%d ", numbersym);
      fprintf(f, "%d ", num);
      goto begin_check;
    }
    // end detect numbers

    // begin detect numbers
    if ( isalpha( ch = source[index] ) ) {
      ident_name[0] = ch;
      ident_name_len = 1;
      index++;
      while ( (index < code_length) && isalnum( ch = source[index]) ) {
        ident_name[ ident_name_len ] = ch;
        ident_name_len++;
        index++;
        if ( ident_name_len > 11 ) {
          printf("LEX ERROR: Variable name is too long\n");
          fclose(f);
          return;
        };
      }
      ident_name[ ident_name_len ] = '\0';
      printf("%-12s %d\n", ident_name, identsym);
      fprintf(f, "%d ", identsym);
      fprintf(f, "%s ", ident_name);
      goto begin_check;
    }
    // end detect numbers

    // begin detect white space
    if ( isspace(ch = source[index]) ) {
      index++;
      goto begin_check;
    }
    // end detect white space

    printf("LEX ERROR: Illegal character.");
    fclose(f);
    return;
}

int main() {
  loadSourceProgram();
  //printSourceProgram();
  //printCleanSourceProgram();
  lexer();
  return 0;
}

