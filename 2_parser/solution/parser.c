#define MAX_SYMBOL_TABLE_SIZE

#include <stdio.h>
#include <malloc.h>

#include "token_type.h"
#include "parser_codegen.h"

static FILE *f;

static int token;

typedef struct symbol {
  int kind;       // const = 1, var = 2, proc = 3
  char name[12];  // name up to 11 characters
  int val;        // value (only for a constant)
  int level;      // L level
  int addr        // M address
} symbol;

symbol symbolTable[MAX_SYMBOL_TABLE_SIZE];

// open and close lexemetable.txt

void openLexemeTable() {
  f = fopen("lexemetable.txt", "r");
  if (f == NULL) {
    printf("\nerror: failed to open lexemetable.txt\n");
    exit(0);
  }
}

void closeLexemTable() {
  fclose(f);
}

// read tokens from file

void getToken() {
  fscanf(f, "%d", &token);

  if (feof(f)) {
    printf("\nerror: unexpected end of file");
    exit(0);
  }
  // unexpected end of file

  printf(" %d", token);
  // TO DO!!!
  if (token == identsym) { printf(" %s", getIdent()); }
  if (token == numbersym){ printf(" %d", getNumber()); }
}

int getNumber() {
  int num;
  fscanf(f, "%d", &num);
  return num;
}

char *getIdent() {
  char *ident = malloc(12);
  fscanf(f, "%s", ident);
  return ident;
}

//

void reportParseError(char* parseErrorMessage) {
  printf("\nerror: %s\n", parseErrorMessage);
  exit(0);
}

// symbol table



// parser and code generator

// program
void program() {
  getToken();
  block();
  if (token != periodsym) reportParseError("expected '.' at the end of program");
  printf("\ntiny PL/0 program is grammatically correct\n");
}

// block
void block() {
  // start: constant declaration
  if (token == constsym) {
    do {
      getToken();
      if (token != identsym)    reportParseError("expected identifier in constant declaration");
      getToken();
      if (token != eqsym)       reportParseError("expected '=' after identifier in constant declaration");
      getToken();
      if (token != numbersym)   reportParseError("expected number after '=' in constant declaration");
      // TO DO: add to symbol table
      getToken();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of constant declaration");
    getToken();
  }
  // end: constant declaration

  // start: variable declaration
  if (token == varsym) {
    do {
      getToken();
      if (token != identsym)    reportParseError("expected identifier in variable declaration");
      getToken();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of variable declaration");
    getToken();
  }
  // end: variable declaration

  statement();
}

void statement() {
  // begin: assignment statement
  if (token == identsym) {
    getToken();
    if (token != becomessym) reportParseError("expected ':=' after identifier in assignment");
    getToken();
    expression();
  }
  // end: assignment statement
  // start: begin ... end
  else if (token == beginsym) {
    getToken();
    statement();
    while (token == semicolonsym) {
      getToken();
      statement();
    }
    if (token != endsym)    reportParseError("expected 'end' at the end of statement");
    getToken();
  }
  // end: begin ... end
  // begin: if statement
  else if (token == ifsym) {
    getToken();
    condition();
    if (token != thensym)   reportParseError("expected 'then' after condition in if statement");
    getToken();
    statement();
  }
  // end: if statement
  // begin: while statement
  else if (token == whilesym) {
    getToken();
    condition();
    if (token != dosym)     reportParseError("expected 'do' after condition in while statement");
    getToken();
    statement();
  }
  // end: while statement
  // start: read statement
  else if (token == readsym) {
    getToken();
    if (token != identsym)  reportParseError("expected identifier after 'read'");
    getToken();
  }
  // end: read statement
  // start: write
  else if (token == writesym) {
    getToken();
    if (token != identsym)  reportParseError("expected identifier after 'write'");
    getToken();
  }
  // end: write
}

void condition() {
  // start: odd symbol
  if (token == oddsym) {
    getToken();
    expression();
  }
  // end: odd symbol
  // start: condition
  else {
    expression();
    if ((token != eqsym) && (token != neqsym) && (token != lessym) && (token != leqsym) && (token != gtrsym) && (token != geqsym)) {
      reportParseError("expected '=', '<>', '<', '<=', '>', or '>=' after expression in condition");
    }
    getToken();
    expression();
  }
  // end: condition
}

void expression() {
  if ((token == plussym) || (token == minussym)) getToken();
  term();
  while ((token == plussym) || (token == minussym)) {
    getToken();
    term();
  }
}

void term() {
  factor();
  while ((token == multsym) || (token == slashsym)) {
    getToken();
    factor();
  }
}

/////////////////////////////////////////////////////////////

void factor() {
  if (token == identsym) {
    getToken();
  }
  else if (token == numbersym) {
    getToken();
  }
  else if (token == lparentsym) {
    getToken();
    expression();
    if (token != rparentsym) reportParseError("expected ')' after '(' and expression in factor");
    getToken();
  }
  else reportParseError("unexpected token in factor");
}


int main() {
  openLexemeTable();
  program();
  closeLexemTable();
}

