#define MAX_SYMBOL_TABLE_SIZE 1000
#define MAX_CODE_LENGTH 1000

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "token_type.h"
#include "parser_codegen.h"

#include "opcode_type.h"

static FILE *f;

static FILE *codeFile;

// token

static int token;
static char *ident;
static int number;

// symbol table

static int numberOfSymbols = 0;

typedef struct symbol {
  int kind;       // const = 1, var = 2, proc = 3
  char *name;     // name up to 11 characters
  int val;        // value (only for a constant)
  int level;      // L level
  int addr        // M address
} symbol_t;

symbol_t symbolTable[MAX_SYMBOL_TABLE_SIZE];

// code index

static int cx = 0;

// code

// instruction type
typedef struct {
	int op;
	int l;
	int m;
} instruction_t;

// code store
instruction_t code[ MAX_CODE_LENGTH ];

// methods

void checkIfUndeclaredIdentifier(symbol_t *pSymbol) {
  if (pSymbol == NULL) {
    printf("\nerror: identifier %s not declared");
    exit(0);
  }
}

void reportParseError(char* parseErrorMessage) {
  printf("\nerror: %s\n", parseErrorMessage);
  exit(0);
}

symbol_t *getSymbol(char *name) {
  int i;
  for (i=0; i < numberOfSymbols; i++)
    if (strcmp(symbolTable[i].name, name) == 0)
      return &symbolTable[i];
  return NULL;
}

void putSymbol(int kind, char *name, int val, int level, int addr) {
  symbol_t newSymbol;
  if (getSymbol(name) != NULL) { printf("\nerror: constant or variable with name %s already declared\n", name); exit(0); }
  newSymbol.kind  = kind;
  newSymbol.name  = name;
  newSymbol.val   = val;
  newSymbol.level = level;
  newSymbol.addr  = addr;
  symbolTable[numberOfSymbols++] = newSymbol;
};

void printSymbolTable() {
  int i;
  for (i = 0; i < numberOfSymbols; i++) {
    printf(" kind :   %d\n", symbolTable[i].kind);
    printf(" name :   %s\n", symbolTable[i].name);
    printf(" val  :   %d\n", symbolTable[i].val);
    printf(" level:   %d\n", symbolTable[i].level);
    printf(" addr :   %d\n", symbolTable[i].addr);
    printf("------------------\n");
  }
}

void printCode() {
  int x;
  printf("cx: %d\n", cx);
  for (x = 0; x < cx; x++) {
    printf("%d %d %d\n", code[x].op, code[x].l, code[x].m);
  }
}

void saveCode() {
  int x;
  printf("cx: %d\n", cx);
  for (x = 0; x < cx; x++) {
    fprintf(codeFile, "%d %d %d\n", code[x].op, code[x].l, code[x].m);
  }
}

// open and close lexemetable.txt

void openLexemeTable() {
  f = fopen("lexemetable.txt", "r");
  if (f == NULL) {
    reportParseError("\nerror: failed to open lexemetable.txt\n");
    exit(0);
  }
}

void closeLexemTable() {
  fclose(f);
}

void openCodeFile() {
  codeFile = fopen("mcode.txt", "w");
  if (codeFile == NULL) {
    printf("\nerror: failed to open mcode.txt");
    exit(0);
  }
};

void closeCodeFile() {
  fclose(codeFile);
}

// read tokens from file

void getToken() {
  fscanf(f, "%d", &token);

  if (feof(f)) {
    reportParseError("\nerror: unexpected end of file");
    exit(0);
  }
  // unexpected end of file

  printf(" %d", token);
  // TO DO!!!
  if (token == identsym) { ident = getIdent(); printf(" %s", ident); }
  if (token == numbersym){ number = getNumber(); printf(" %d", number); }
}

int getNumber() {
  int num;
  fscanf(f, "%d", &num);
  return num;
}

char *getIdent() {
  char *ident = (char *) malloc(12);
  fscanf(f, "%s", ident);
  return ident;
}

void emit(int op, int l, int m) {
  printf("\nemitting code: %d %d %d\n", op, l, m);
  if (cx >= MAX_CODE_LENGTH) {
    reportParseError("generated code is too long");
  }
  code[cx].op = op;
  code[cx].l  = l;
  code[cx].m  = m;
  cx++;
}

// parser and code generator

// program
void program() {
  getToken();
  block();
  if (token != periodsym) reportParseError("expected '.' at the end of program");
  printf("\ntiny PL/0 program is grammatically correct\n");
  // emit code for halting the machine
  emit(SIO, 0, 2);
}

// block
void block() {
  int numberOfVariables = 0;

  // start: constant declaration
  if (token == constsym) {
    do {
      getToken();
      if (token != identsym)    reportParseError("expected identifier in constant declaration");
      getToken();
      if (token != eqsym)       reportParseError("expected '=' after identifier in constant declaration");
      getToken();
      if (token != numbersym)   reportParseError("expected number after '=' in constant declaration");
      // put constant into symbol table
      putSymbol(1, ident, number, 0, 0);
      getToken();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of constant declaration");
    getToken();
  }
  // end: constant declaration

  // start: variable declaration
  if (token == varsym) {
    do {
      getToken();
      if (token != identsym)    reportParseError("expected identifier in variable declaration");
      numberOfVariables++;
      // put variable into symbol table
      putSymbol(2, ident, 0, 0, 3 + numberOfVariables);
      getToken();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of variable declaration");
    getToken();
  }
  // end: variable declaration
  emit(INC, 0, 4 + numberOfVariables); // 4 because of functional value, static link, dynamic link, return address
  statement();
}

void statement() {
  // begin: assignment statement
  if (token == identsym) {
    // save variable name
    char *variableName = ident;
    getToken();
    if (token != becomessym) reportParseError("expected ':=' after identifier in assignment");
    getToken();
    expression();
    // start: emit code for variable assignment
    symbol_t *pVariableSymbol = getSymbol(variableName);
    printf("\nvariable name: %s\n", variableName);
    checkIfUndeclaredIdentifier(pVariableSymbol);
    printf("psymbol->name: %s\n", pVariableSymbol->name);
    emit(STO, 0, pVariableSymbol->addr);
    // end: emit code for variable assignment
  }
  // end: assignment statement
  // start: begin ... end
  else if (token == beginsym) {
    getToken();
    statement();
    while (token == semicolonsym) {
      getToken();
      statement();
    }
    if (token != endsym)    reportParseError("expected 'end' at the end of statement");
    getToken();
  }
  // end: begin ... end
  // begin: if statement
  else if (token == ifsym) {
    getToken();
    condition();
    if (token != thensym)   reportParseError("expected 'then' after condition in if statement");
    int ctemp = cx;
    emit(JPC, 0, 0);
    getToken();
    statement();
    code[ctemp].m = cx;
  }
  // end: if statement
  // begin: while statement
  else if (token == whilesym) {
    int cx1 = cx;
    getToken();
    condition();
    int cx2 = cx;
    emit(JPC, 0, 0);
    if (token != dosym)     reportParseError("expected 'do' after condition in while statement");
    getToken();
    statement();
    emit(JMP, 0, cx1);
    code[cx2].m = cx;
  }
  // end: while statement
  // start: read statement
  else if (token == readsym) {
    getToken();
    if (token != identsym)  reportParseError("expected identifier after 'read'");
    // begin: emit code to write
    char *variableName = ident;
    symbol_t *pVariableSymbol = getSymbol(variableName);
    checkIfUndeclaredIdentifier(pVariableSymbol);
    // check if constant
    if (pVariableSymbol->kind == 1) {
        printf("\nerror: 'read' is followed by a constant\n");
    }
    //printf("\nvariable name: %s\n", variableName);
    //printf("psymbol->name: %s\n", pVariableSymbol->name);
    emit(SIO, 0, 1);
    emit(STO, 0, pVariableSymbol->addr);
    // end: emit code to read variable
    getToken();
  }
  // end: read statement
  // start: write
  else if (token == writesym) {
    getToken();
    if (token != identsym)  reportParseError("expected identifier after 'write'");
    // start: emit code to output variable
    char *identName = ident;
    symbol_t *pIdentSymbol = getSymbol(identName);
    checkIfUndeclaredIdentifier(pIdentSymbol);
    //printf("\nvariable name: %s\n", variableName);
    //printf("psymbol->name: %s\n", pVariableSymbol->name);
    if (pIdentSymbol->kind == 2) {
      emit(LOD, 0, pIdentSymbol->addr);
      emit(SIO, 0, 0);
    }
    else {
      emit(LIT, 0, pIdentSymbol->val);
      emit(SIO, 0, 0);
    }
    // end: emit code to output variable
    getToken();
  }
  // end: write
}

void condition() {
  // start: odd symbol
  if (token == oddsym) {
    // start: emit code
    emit(OPR, 0, ODD);
    // end: emit code
    getToken();
    expression();
  }
  // end: odd symbol
  // start: condition
  else {
    expression();
    if ((token != eqsym) && (token != neqsym) && (token != lessym) && (token != leqsym) && (token != gtrsym) && (token != geqsym)) {
      reportParseError("expected '=', '<>', '<', '<=', '>', or '>=' after expression in condition");
    }
    token_type relationToken = token;
    getToken();
    expression();
    // start: emit code
    switch (relationToken) {
      case eqsym  : { emit(OPR, 0, EQL); break; }
      case neqsym : { emit(OPR, 0, NEQ); break; }
      case lessym : { emit(OPR, 0, LSS); break; }
      case leqsym : { emit(OPR, 0, LEQ); break; }
      case gtrsym : { emit(OPR, 0, GTR); break; }
      case geqsym : { emit(OPR, 0, GEQ); break; }
    }
    // end: emit code
  }
  // end: condition
}

void expression() {
  if ((token == plussym) || (token == minussym)) {
    token_type plusOrMinusToken = token;
    getToken();
    term();
    if (plusOrMinusToken == minussym) {
      emit(OPR, 0, NEG);
    }
  }
  else {
    term();
  }

  while ((token == plussym) || (token == minussym)) {
    token_type plusOrMinusToken = token;
    getToken();
    term();
    if (plusOrMinusToken == plussym) {
      emit(OPR, 0, ADD);
    }
    else {
      emit(OPR, 0, SUB);
    }
  }
}

void term() {
  factor();
  while ((token == multsym) || (token == slashsym)) {
    token_type multOrDivToken = token;
    getToken();
    factor();
    if (multOrDivToken == multsym) {
      emit(OPR, 0, MUL);
    }
    else {
      emit(OPR, 0, DIV);
    }
  }
}

/////////////////////////////////////////////////////////////

void factor() {
  if (token == identsym) {
    // check if this is a constant or a variable
    symbol_t *pConstantOrVariableSymbol = getSymbol(ident);
    printf("\nchecking if %s is constant or variable\n", ident);
    // begin: emit code
    if (pConstantOrVariableSymbol->kind == 1) {
      // constant
      printf("\nconstant %s with value %d in factor\n", pConstantOrVariableSymbol->name, pConstantOrVariableSymbol->val);
      emit(LIT, 0, pConstantOrVariableSymbol->val);
    }
    else {
      // variable
      emit(LOD, 0, pConstantOrVariableSymbol->addr);
    }
    // end: emit code
    getToken();
  }
  else if (token == numbersym) {
    // start: emit code to push constant onto stack
    emit(LIT, 0, number);
    // end: emit code to push constant onto stack
    getToken();
  }
  else if (token == lparentsym) {
    getToken();
    expression();
    if (token != rparentsym) reportParseError("expected ')' after '(' and expression in factor");
    getToken();
  }
  else reportParseError("unexpected token in factor");
}


int main() {
  openLexemeTable();
  program();
  closeLexemTable();
  printSymbolTable();

  printCode();
  openCodeFile();
  saveCode();
  closeCodeFile();
  return 0;
}

