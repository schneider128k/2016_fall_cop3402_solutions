#include <stdio.h>

// constants
#define MAX_CODE_LENGTH 500
#define MAX_STACK_HEIGHT 2000
#define MAX_LEXI_LEVELS 3

// instruction type
typedef struct {
	int op;
	int l;
	int m;
} instruction_t;

// operation names (e.g.,LIT,OPR) corresponding to the opcodes (e.g.,01,02) given in the input file
char *opcode_name[12] = {
 "   ",
 "LIT", // 01
 "OPR", // 02
 "LOD", // 03
 "STO", // 04
 "CAL", // 05
 "INC", // 06
 "JMP", // 07
 "JPC", // 08
 "SIO", // 09
 "SI0", // 10
 "SIO"  // 11
};

// arithmetical logical operator (alo) names (e.g., RET, NEG) corresponding to the modifier values (e.g., 0,1) given in the input file
char *alo_name[14] = {
  "RET", //  0
  "NEG", //  1
  "ADD", //  2
  "SUB", //  3
  "MUL", //  4
  "DIV", //  5
  "ODD", //  6
  "MOD", //  7
  "EQL", //  8
  "NEQ", //  9
  "LSS", // 10
  "LEQ", // 11
  "GTR", // 12
  "GEQ"  // 13
};

// code store
instruction_t code[ MAX_CODE_LENGTH ];

// stack store
int stack[ MAX_STACK_HEIGHT ];

// code length
int code_length;

// registers

// program counter
int pc;

// base pointer - points to the base of the current activation record on the stack
int bp;

// stack pointer - points to the top of the stack
int sp;

// instruction register
instruction_t ir;

// the current number of activation records
int ar_count;

// input and halt flags
int io_flag;
int halt_flag;

// the list of positions at which the different activation records start
int ar_position_list[ MAX_LEXI_LEVELS ];
///////////////////////////////////////////////////////////////////////////////////
// print to StackTrace.txt file
void printToFileAndScreen(char* s, int d) {
    printf("%s", s);
    if(d!=-1){
        printf("%d", d);
        if(d<10){
         printf(" ");
        }
    }
    FILE *f = fopen("stacktrace.txt", "a"); // append mode
    if (f == NULL)
    {
        printf("Error reading output file !\n");
        return;
    }
    fprintf(f, "%s", s);
    // formatting for visual purpose
    if(d!=-1){
        fprintf(f, "%d", d);
        if(d<10){
         fprintf(f, " ");
        }
    }
    fclose(f);
};
void printToFile(char* s, int d) {
    FILE *f = fopen("stacktrace.txt", "a"); // append mode
    if (f == NULL)
    {
        printf("Error reading output file !\n");
        return;
    }
    fprintf(f, "%s", s);
    // formatting for visual purpose
    if(d!=-1){
        fprintf(f, "%d", d);
        if(d<10){
         fprintf(f, " ");
        }
    }
    fclose(f);
};

// printInstruction
void printInstruction( int line, instruction_t instruction ) {
  printToFileAndScreen("",line);
  printToFileAndScreen(" ", instruction.op);
  printToFileAndScreen(" ", instruction.m);
  printToFileAndScreen(" ", instruction.l);
};

// printSymbolicInstruction
void printSymbolicInstruction(int line, instruction_t instruction) {
  printToFileAndScreen("", line);
  printToFileAndScreen("   ", -1);
  printToFileAndScreen(opcode_name[ instruction.op], -1);
  printToFileAndScreen("  ", instruction.l);
  printToFileAndScreen("  ", instruction.m);
  printToFileAndScreen("  ",-1);
};

void printInputOutput(){
    if(io_flag == 2){
       printToFileAndScreen("Output: ", stack[ sp+1 ]);
       printToFileAndScreen("\n", -1);
    }
    // resetting the flag (no need to print anything in the input case)
    io_flag=0;
}

// printMachineState
void printMachineState() {
  int s;
  int ar_index;

  ar_index = 1;
  if(io_flag == 1){
    printToFileAndScreen("                  ",-1); // visual formatting
  }
  printToFileAndScreen("   ",pc);
  printToFileAndScreen("    ",bp);
  printToFileAndScreen("    ",sp);
    printToFileAndScreen("   ",-1);
  for ( s = 1; s <= sp; s++ ) {
    if ( s == ar_position_list[ ar_index ] ) {
      printToFileAndScreen( "| ", -1);
      ar_index++;
    }
    printToFileAndScreen( "", stack[ s ] );
  }
 printToFileAndScreen("\n", -1);
}

void initialize() {
  // initialize registers
  sp = 0;
  bp = 1;
  pc = 0;
  ir.op = 0;
  ir.l  = 0;
  ir.m  = 0;

  ar_position_list[ 0 ] = 0;
  ar_count = 1;
   // io_flag is used for visual formatting of user input and output
  io_flag = 0;
  halt_flag = 0;

  FILE *f = fopen("stacktrace.txt", "w"); // create mode
  if (f == NULL)
  {
    printf("Error creating output file !\n");
    return;
  }
};

// load code
void loadCode() {
  FILE *input = fopen( "mcode.txt", "r" );
  int line;
  int op, m, l;
  instruction_t instruction;

  line = 0;
  printToFileAndScreen("Line OP   L   M \n", -1);
  printToFileAndScreen("-----------------\n", -1);

  while(1) {
  	fscanf( input, "%d", &op );

  	if (feof( input) ) break;

  	fscanf( input, "%d", &l );
  	fscanf( input, "%d", &m );

    instruction.op = op;
    instruction.m  = m;
    instruction.l  = l;

    code[ line ] = instruction;

    //printInstruction( line, instruction );
    printSymbolicInstruction( line, code[ line ] );
    printToFileAndScreen("\n", -1);

    line++;

  };

  code_length = line;

  fclose( input );
}

// fetchInstruction
void fetchInstruction() {
  ir = code[ pc ];
  printSymbolicInstruction( pc, ir );
  //printf( "\n" );
  pc++;
}

// base
int base( level, b ) {
  while ( level > 0 ) {
    b = stack[ b + 2 ];
    level--;
  }
  return b;
}

// performArithmeticOrLogicalOperation
void performArithmeticOrLogicalOperation() {
  switch ( ir.m ) {
    // 0 RET
    case 0 :
      ar_count--;
      //
      sp = bp - 1;
      pc = stack[ sp + 4];
      bp = stack[ sp + 3];
      break;

    // 1 NEG
    case 1 :
      stack[ sp ] = - stack[ sp ];
      break;

    // 2 ADD
    case 2 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] + stack[ sp + 1];
      break;

    // 3 SUB
    case 3 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] - stack[ sp + 1];
      break;

    // 4 MUL
    case 4 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] * stack[ sp + 1];
      break;

    // 5 DIV
    case 5 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] / stack[ sp + 1];
      break;

    // 6 ODD
    case 6 :
      stack[ sp ] = stack[ sp ] % 2;
      break;

    // 7 MOD
    case 7 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] % stack[ sp + 1];
      break;

    // 8 EQL
    case 8 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] == stack[ sp + 1 ] );
      break;

    // 9 NEQ
    case 9 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] != stack[ sp + 1 ] );
      break;

    // 10 LSS
    case 10 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] < stack[ sp + 1 ] );
      break;

    // 11 LEQ
    case 11 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] <= stack[ sp + 1 ] );
      break;

    // 12 GTR
    case 12 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] > stack[ sp + 1 ] );
      break;

    // 13 GEQ
    case 13 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] >= stack[ sp + 1 ] );
      break;
  }
}

void performInputOutputOrHaltOperation() {
  switch ( ir.m ) {
    // SIO 0, 0
    // write the top stack element to the screen
    case 0 :
      sp = sp - 1;
      io_flag = 2;// flag for output, used for visual purpose
      break;

    // SIO 0, 1
    // read in input from the user and store it at the top of the stack
    case 1 :
      printToFileAndScreen("\nPlease enter an integer input: ", -1);
      int input;
      scanf("%d", &input);
      sp = sp + 1;
      stack[ sp ] = input;
      io_flag = 1;// flag for input, used for visual purpose
      printToFile("", input);
      printToFile("\n", -1);
      break;
    // SIO 0, 2
    // halt the machine
    case 2 :
      halt_flag=1;
      break;
  }
  return;
}

// executeInstruction
void executeInstruction() {
  switch ( ir.op ) {

    // LIT 0, M
    // push constant value (literal) onto stack
    case 1 :
      sp++;
      stack[ sp ] = ir.m;
      break;

    // OPR 0, M
    // perform arithmetic or logical operation on data on the top of stack
    case 2 :
      performArithmeticOrLogicalOperation();
      break;

    // LOD L, M
    // load value into top of stack from stack location at offset M from L lexicographical levels down
    case 3 :
      sp++;
      stack[ sp ] = stack[ base( ir.l, bp ) + ir.m ];
      break;

    // STO L, M
    // store value at top stack in the stack location at offset M from L lexicographical levels down
    case 4 :
      stack[ base( ir.l, bp ) + ir.m ] = stack[ sp ];
      sp--;
      break;

    // CAL L, M
    // call procedure at code index M (generates new Activation Record and pc = M)
    case 5 :
      ar_position_list[ ar_count ] = sp + 1;
      ar_count++;
      stack[ sp + 1 ] = 0;                // space to return functional value (FV)
      stack[ sp + 2 ] = base( ir.l, bp ); // static link (SL)
      stack[ sp + 3 ] = bp;               // dynamic link (DL)
      stack[ sp + 4 ] = pc;               // return address
      bp = sp + 1;
      pc = ir.m;
      break;

    // INC 0, M
    // allocate M locals (increment sp by M); first four are functional value, static link, dynamic link, and return address
    case 6 :
      sp = sp + ir.m;
      break;

    // JMP 0, M
    // jump to instruction at M
    case 7 :
      pc = ir.m;
      break;

    // JPC 0, M
    // jump to instruction M if top stack element is 0
    case 8 :
      if ( stack[ sp ] == 0 ) {
        pc = ir.m;
      }
      sp--;
      break;

    // SIO 0, 1
    // write the top stack element to the screen
    case 9 :
      return performInputOutputOrHaltOperation();
  }
  return;
}
void printInitialState(){\
  printToFileAndScreen( "\n",-1 );
  printToFileAndScreen( "Line OP   L    M     pc    bp    sp   Stack\n",-1);
  printToFileAndScreen( "--------------------------------------------\n",-1);
  printToFileAndScreen( "Initial values    ",-1);
  printMachineState();
  printToFileAndScreen("\n", -1);
}

// main
int main() {
  initialize();
  loadCode();
  printInitialState();
  do{
    fetchInstruction();
    executeInstruction();
    printMachineState();\
    if(io_flag == 1 || io_flag == 2){
        printInputOutput();
    }
  } while(halt_flag ==0);
  return 0;
}
