//
// lexical tokens: 
// identifiers letter (digit | letter)*
// plus, mult, left paren, right paren

// I follow the conventions of lex
// the token type is returned by the lex function
// the token value (if any) is availalbe in the global variable lval

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "util.h"
#include "tokens.h"

LVAL lval;
token_type token;

static FILE *source_file_p;

static struct {
  string lexeme;
  token_type type;
} keywords[14] = {
  "begin", beginsym, 
  "end", endsym, 
  "odd", oddsym,
  "if", ifsym, 
  "then", thensym,
  "while", whilesym, 
  "do", dosym, 
  "call", callsym, 
  "const", constsym, 
  "var", varsym, 
  "procedure", procsym, 
  "write", writesym,
  "read", readsym , 
  "else", elsesym
};

void open_source_file(string source_file_name) {
  source_file_p = fopen(source_file_name, "r");
  if (source_file_p == NULL) {
    printf("failed to open source file %s\n", source_file_name);
    exit(1);
  }
  else {
    printf("opened source file %s\n", source_file_name);
  }
}

void close_source_file() {
  fclose(source_file_p);
}

token_type lex() {
  char c;

  // TO DO: ignore comments

  // ignore space, tab, newline
  while ((c=fgetc(source_file_p)) == ' ' || c== '\t' || c == '\n')
    ; 
  if (c == EOF) return nulsym;

  // identifier
  if (isalpha(c)) {
    char sbuf[100], *p = sbuf;
    do {
      *p++ = c;
    } while ((c=fgetc(source_file_p)) != EOF && isalnum(c));
    ungetc(c, source_file_p);
    *p = '\0';
    // check if it is a keyword
    for (int i = 0; i < 14; i++) 
      if (strcmp(sbuf, keywords[i].lexeme) == 0) return keywords[i].type;
    // it is an indentifier
    lval.id = String(sbuf); 
    return identsym;
  }

  // number
  // TO DO: 2num identifier starts with number
  if (isdigit(c)) {
    ungetc(c, source_file_p);
    fscanf(source_file_p, "%d", &lval.num);
    return numbersym;
  } 

  switch (c) {
  case '+' :
    return plussym;
  case '-' :
    return minussym;
  case '*' :
    return multsym;
  case '/' :
    return slashsym;
  case '(' :
    return lparentsym;
  case ')' :
    return rparentsym;
  case ';' :
    return semicolonsym;
  case ',' :
    return commasym;
  case '=' :
    return eqsym;
  case '.' :
    return periodsym;
  case ':' :
    c = fgetc(source_file_p);
    if (c == '=') return becomessym;
    printf("Illegal token; : should be followed by = \n");
    exit(1);
  case '<' :
    c = fgetc(source_file_p);
    if (c == '>') {
      return neqsym;
    }
    else if (c == '=') {
      return leqsym;
    }
    else {
      ungetc(c, source_file_p);
      return lessym;
    } 
  case '>' :
    c = fgetc(source_file_p);
    if (c == '=') {
      return geqsym;
    }
    else {
      ungetc(c, source_file_p);
      return gtrsym;
    }
  }

  printf("illegal token\n");
  exit(1);
}

void advance() {
  token = lex();
  //printf("%d\n", token);
}

