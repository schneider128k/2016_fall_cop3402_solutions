#include "util.h"

void open_source_file(string source_file_name);
void close_source_file();

token_type lex();
void advance();
