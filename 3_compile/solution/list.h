#define DATA(L) ((L)->datapointer)
#define NEXT(L) ((L)->next)

typedef enum { OK, ERROR } status;
typedef struct node node, *list;

struct node {
  void *datapointer;
  list next;
};
