#ifndef OPCODE_TYPE
#define OPCODE_TYPE

typedef enum {
    LIT = 1, // 1
    OPR, // 2
    LOD, // 3
    STO, // 4
    CAL, // 5
    INC, // 6
    JMP, // 7
    JPC, // 8
    SIO  // 9
} opcode_type;

typedef enum {
  RTN, // 0
  NEG, // 1
  ADD, // 2
  SUB, // 3
  MUL, // 4
  DIV, // 5
  MOD, // 6
  ODD, // 7
  EQL, // 8
  NEQ, // 9
  LSS, // 10
  LEQ, // 11
  GTR, // 12
  GEQ  // 13
} opr_type;

#endif // OPCODE_TYPE
