#include <stdio.h>
#include <stdlib.h>
#include "tokens.h"
#include "lex.h"
#include "util.h"
#include "parser.h"
#include "symbol_table.h"

extern token_type token;
extern LVAL lval;

proc_type *current_proc;

void reportParseError(string parseErrorMessage) {
  printf("\nerror: %s\n", parseErrorMessage);
  exit(0);
}

// parser 

// program
void program() {
  advance();
  print_proc(current_proc);
  block();
  if (token != periodsym) reportParseError("expected '.' at the end of program");
  printf("\ntiny PL/0 program is grammatically correct\n");
}

// block
void block() {
  // start: constant declaration
  if (token == constsym) {
    do {
      advance();
      if (token != identsym)    reportParseError("expected identifier in constant declaration");
      string id = lval.id;
      advance();
      if (token != eqsym)       reportParseError("expected '=' after identifier in constant declaration");
      advance();
      if (token != numbersym)   reportParseError("expected number after '=' in constant declaration");
      // add const to symbol table
      ident_type *ident;
      ident = make_constant(id, lval.num);
      put_ident(current_proc, ident); 
      
      advance();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of constant declaration");
    advance();
  }
  // end: constant declaration

  // start: variable declaration
  if (token == varsym) {
    int num_vars = 0;
    do {
      advance();
      if (token != identsym)    reportParseError("expected identifier in variable declaration");
      ident_type *ident = make_variable(lval.id, current_proc->level, 4 + num_vars);
      put_ident(current_proc, ident);
      num_vars++;
      advance();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of variable declaration");
    advance();
  }
  // end: variable declaration

  // begin: procedure declaration
  while (token == procsym) {
    advance();
    if (token != identsym) reportParseError("expected identifier in procedure declaration");
    proc_type *parent = current_proc;
    proc_type *child = make_proc(lval.id, parent);
    put_proc(parent, child);
    advance();
    if (token != semicolonsym) reportParseError("';' expected after procedure name");
    advance();
    current_proc = child;
    print_proc(current_proc);
    block();
    current_proc = parent;
    if (token != semicolonsym) reportParseError("';' expected after procedure body");
    advance();
  }
  // end: procedure declaration

  statement();
}

void statement() {
  // begin: assignment statement
  if (token == identsym) {
    ident_type *ident = get_ident(current_proc, lval.id);
    if (ident == NULL) {
      printf("undeclared identifier\n");
    } 
    else if (ident->kind == 1) {
      printf("cannot assign value to constant");
    }
    else {
      print_ident(ident);
    }

    advance();
    if (token != becomessym) reportParseError("expected ':=' after identifier in assignment");
    advance();
    expression();
  }
  // end: assignment statement
  // start: begin ... end
  else if (token == beginsym) {
    advance();
    statement();
    while (token == semicolonsym) {
      advance();
      statement();
    }
    if (token != endsym)    reportParseError("expected 'end' at the end of statement");
    advance();
  }
  // end: begin ... end
  // begin: if statement
  else if (token == ifsym) {
    advance();
    condition();
    if (token != thensym)   reportParseError("expected 'then' after condition in if statement");
    advance();
    statement();
  }
  // end: if statement
  // begin: while statement
  else if (token == whilesym) {
    advance();
    condition();
    if (token != dosym)     reportParseError("expected 'do' after condition in while statement");
    advance();
    statement();
  }
  // end: while statement
  // start: read statement
  else if (token == readsym) {
    advance();
    if (token != identsym)  reportParseError("expected identifier after 'read'");
    advance();
  }
  // end: read statement
  // start: write
  else if (token == writesym) {
    advance();
    if (token != identsym)  reportParseError("expected identifier after 'write'");
    advance();
  }
  // end: write
}

void condition() {
  // start: odd symbol
  if (token == oddsym) {
    advance();
    expression();
  }
  // end: odd symbol
  // start: condition
  else {
    expression();
    if ((token != eqsym) && (token != neqsym) && (token != lessym) && (token != leqsym) && (token != gtrsym) && (token != geqsym)) {
      reportParseError("expected '=', '<>', '<', '<=', '>', or '>=' after expression in condition");
    }
    advance();
    expression();
  }
  // end: condition
}

void expression() {
  if ((token == plussym) || (token == minussym)) advance();
  term();
  while ((token == plussym) || (token == minussym)) {
    advance();
    term();
  }
}

void term() {
  factor();
  while ((token == multsym) || (token == slashsym)) {
    advance();
    factor();
  }
}

/////////////////////////////////////////////////////////////

void factor() {
  if (token == identsym) {
    advance();
  }
  else if (token == numbersym) {
    advance();
  }
  else if (token == lparentsym) {
    advance();
    expression();
    if (token != rparentsym) reportParseError("expected ')' after '(' and expression in factor");
    advance();
  }
  else reportParseError("unexpected token in factor");
}


int main(int argc, string argv[]) {
  open_source_file(argv[1]);

  current_proc = make_proc("main", NULL);
  printf("here");
  // parse
  program();
  //print_idents(start);
  close_source_file();
}

