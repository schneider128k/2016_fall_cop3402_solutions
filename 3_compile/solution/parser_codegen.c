#define MAX_SYMBOL_TABLE_SIZE 1000
#define MAX_CODE_LENGTH 1000

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "tokens.h"
#include "lex.h"
#include "parser.h"
#include "opcode_type.h"
#include "symbol_table.h"

// lexer stuff

// token and semantic value

extern int token;
extern LVAL lval;

// code generation

// current procedure

static proc_type *current_proc; 

// code file

static FILE *codeFile;

// code index

static int cx = 0;

// code

// instruction type
typedef struct {
	int op;
	int l;
	int m;
} instruction_t;

// code store
instruction_t code[ MAX_CODE_LENGTH ];

// methods

void reportParseError(char* parseErrorMessage) {
  printf("\nerror: %s\n", parseErrorMessage);
  exit(0);
}

void printCode() {
  int x;
  printf("cx: %d\n", cx);
  for (x = 0; x < cx; x++) {
    printf("%d %d %d\n", code[x].op, code[x].l, code[x].m);
  }
}

void saveCode() {
  int x;
  printf("cx: %d\n", cx);
  for (x = 0; x < cx; x++) {
    fprintf(codeFile, "%d %d %d\n", code[x].op, code[x].l, code[x].m);
  }
}

void openCodeFile(string code_file_name) {
  codeFile = fopen(code_file_name, "w");
  if (codeFile == NULL) {
    printf("failed to open %s\n", code_file_name);
    exit(0);
  }
};

void closeCodeFile() {
  fclose(codeFile);
}

void emit(int op, int l, int m) {
  printf("\nemitting code: %d %d %d\n", op, l, m);
  if (cx >= MAX_CODE_LENGTH) {
    reportParseError("generated code is too long");
  }
  code[cx].op = op;
  code[cx].l  = l;
  code[cx].m  = m;
  cx++;
}

// parser and code generator

// program
void program() {
  current_proc = make_proc("main", NULL);

  // get the first token of the program
  advance();
  block();

  if (token != periodsym) reportParseError("expected '.' at the end of program");
  printf("\ntiny PL/0 program is grammatically correct\n");
  // halt
  emit(SIO, 0, 2);
}

// block

void block() {
  int ctemp = cx;
  emit(JMP, 0, 0); // needs to be adjusted later

  // constants
  const_declaration();

  // variables 
  int num_vars = var_declaration();

  // procedures
  proc_declaration();

  // fix address of jmp
  code[ctemp].m = cx;

  // inc
  emit(INC, 0, 4 + num_vars);  

  // statement
  statement();
}

void const_declaration() {
  // start: constant declaration
  if (token == constsym) {
    do {
      advance();
      if (token != identsym)    reportParseError("expected identifier in constant declaration");
      string id = lval.id;
      advance();
      if (token != eqsym)       reportParseError("expected '=' after identifier in constant declaration");
      advance();
      if (token != numbersym)   reportParseError("expected number after '=' in constant declaration");
      // add const to symbol table
      ident_type *ident;
      ident = make_constant(id, lval.num);
      put_ident(current_proc, ident); 
      
      advance();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of constant declaration");
    advance();
  }
  // end: constant declaration
}

int var_declaration() {
  // start: variable declaration
  int num_vars = 0;  
  if (token == varsym) {
    do {
      advance();
      if (token != identsym)    reportParseError("expected identifier in variable declaration");
      ident_type *ident = make_variable(lval.id, current_proc->level, 4 + num_vars);
      put_ident(current_proc, ident);
      num_vars++;
      advance();
    } while (token == commasym);
    if (token != semicolonsym)  reportParseError("expected ';' at the end of variable declaration");
    advance();
  }
  return num_vars;
  // end: variable declaration
}

void proc_declaration() {
  // begin: procedure declaration
  while (token == procsym) {
    advance();
    if (token != identsym) reportParseError("expected identifier in procedure declaration");
    proc_type *parent = current_proc;
    proc_type *child = make_proc(lval.id, parent);
    put_proc(parent, child);
    advance();
    if (token != semicolonsym) reportParseError("';' expected after procedure name");
    advance();
    current_proc = child;
    print_proc(current_proc);
    // update address 
    current_proc->addr = cx;
    block();
    printf("emitting return\n");
    emit(OPR, 0, RTN);
    current_proc = parent;
    if (token != semicolonsym) reportParseError("';' expected after procedure body");
    advance();
  }
  // end: procedure declaration
}

void statement() {
  // begin: assignment statement
  if (token == identsym) {
    // save variable name
    string variableName = lval.id;
    advance();
    if (token != becomessym) reportParseError("expected ':=' after identifier in assignment");
    advance();
    expression();
    // start: emit code for variable assignment
    ident_type *ident = get_ident(current_proc, variableName);
    if (ident == NULL) reportParseError("undeclared identifier in assignment statement");
    print_ident(ident);
    if (ident->kind == 1) reportParseError("constant on left hand side in assignment statement");
    emit(STO, current_proc->level - ident->level, ident->modifier);
    // end: emit code for variable assignment
  }
  // end: assignment statement
  // begin: call
  if (token == callsym) {
    advance();
    if (token != identsym) {
      reportParseError("identifier expected after 'procedure'");
    }    
    proc_type *callee = get_proc(current_proc, lval.id); 
    if (callee == NULL) {
      reportParseError("unknown procedure name in 'call'");
    }
    // !!! CHECK !!!
    emit(CAL, - (callee->level - current_proc->level - 1), callee->addr);
    advance();
  }
  // end: call
  // start: begin ... end
  else if (token == beginsym) {
    advance();
    statement();
    while (token == semicolonsym) {
      advance();
      statement();
    }
    if (token != endsym)    reportParseError("expected 'end' at the end of statement");
    advance();
  }
  // end: begin ... end
  // begin: if statement
  else if (token == ifsym) {
    advance();
    condition();
    if (token != thensym)   reportParseError("expected 'then' after condition in if statement");
    int ctemp = cx;
    emit(JPC, 0, 0);
    advance();
    statement();
    code[ctemp].m = cx;
  }
  // end: if statement
  // begin: while statement
  else if (token == whilesym) {
    int cx1 = cx;
    advance();
    condition();
    int cx2 = cx;
    emit(JPC, 0, 0);
    if (token != dosym)     reportParseError("expected 'do' after condition in while statement");
    advance();
    statement();
    emit(JMP, 0, cx1);
    code[cx2].m = cx;
  }
  // end: while statement
  // start: read statement
  else if (token == readsym) {
    advance();
    if (token != identsym)  reportParseError("expected identifier after 'read'");
    // begin: emit code to write
    string variableName = lval.id;
    ident_type *ident = get_ident(current_proc, variableName);
    if (ident == NULL) reportParseError("undeclared identifier in 'read'");
    // check if constant
    if (ident->kind == 1) {
        reportParseError("\nerror: 'read' is followed by a constant\n");
    }
    //printf("\nvariable name: %s\n", variableName);
    //printf("psymbol->name: %s\n", pVariableSymbol->name);
    emit(SIO, 0, 1);
    emit(STO, current_proc->level - ident->level, ident->modifier);
    // end: emit code to read variable
    advance();
  }
  // end: read statement
  // start: write
  else if (token == writesym) {
    advance();
    if (token != identsym)  reportParseError("expected identifier after 'write'");
    // start: emit code to output variable
    ident_type *ident = get_ident(current_proc, lval.id);
    if (ident == NULL) {
      reportParseError("undeclared identifier in 'write'");
    }
    //printf("\nvariable name: %s\n", variableName);
    //printf("psymbol->name: %s\n", pVariableSymbol->name);
    if (ident->kind == 2) {
      emit(LOD, current_proc->level - ident->level, ident->modifier);
      emit(SIO, 0, 0);
    }
    else {
      emit(LIT, 0, ident->num);
      emit(SIO, 0, 0);
    }
    // end: emit code to output variable
    advance();
  }
  // end: write
}

void condition() {
  // start: odd symbol
  if (token == oddsym) {
    // start: emit code
    emit(OPR, 0, ODD);
    // end: emit code
    advance();
    expression();
  }
  // end: odd symbol
  // start: condition
  else {
    expression();
    if ((token != eqsym) && (token != neqsym) && (token != lessym) && (token != leqsym) && (token != gtrsym) && (token != geqsym)) {
      reportParseError("expected '=', '<>', '<', '<=', '>', or '>=' after expression in condition");
    }
    token_type relationToken = token;
    advance();
    expression();
    // start: emit code
    switch (relationToken) {
      case eqsym  : { emit(OPR, 0, EQL); break; }
      case neqsym : { emit(OPR, 0, NEQ); break; }
      case lessym : { emit(OPR, 0, LSS); break; }
      case leqsym : { emit(OPR, 0, LEQ); break; }
      case gtrsym : { emit(OPR, 0, GTR); break; }
      case geqsym : { emit(OPR, 0, GEQ); break; }
    }
    // end: emit code
  }
  // end: condition
}

void expression() {
  if ((token == plussym) || (token == minussym)) {
    token_type plusOrMinusToken = token;
    advance();
    term();
    if (plusOrMinusToken == minussym) {
      emit(OPR, 0, NEG);
    }
  }
  else {
    term();
  }

  while ((token == plussym) || (token == minussym)) {
    token_type plusOrMinusToken = token;
    advance();
    term();
    if (plusOrMinusToken == plussym) {
      emit(OPR, 0, ADD);
    }
    else {
      emit(OPR, 0, SUB);
    }
  }
}

void term() {
  factor();
  while ((token == multsym) || (token == slashsym)) {
    token_type multOrDivToken = token;
    advance();
    factor();
    if (multOrDivToken == multsym) {
      emit(OPR, 0, MUL);
    }
    else {
      emit(OPR, 0, DIV);
    }
  }
}

/////////////////////////////////////////////////////////////

void factor() {
  if (token == identsym) {
    // check if this is a constant or a variable
    ident_type *ident = get_ident(current_proc, lval.id);
    if (ident == NULL) {
      reportParseError("undeclared identifier in factor");
    }
    print_ident(ident);
    // begin: emit code
    if (ident->kind == 1) {
      // constant
      emit(LIT, 0, ident->num);
    }
    else {
      // variable
      emit(LOD, current_proc->level - ident->level, ident->modifier);
    }
    // end: emit code
    advance();
  }
  else if (token == numbersym) {
    // start: emit code to push constant onto stack
    emit(LIT, 0, lval.num);
    // end: emit code to push constant onto stack
    advance();
  }
  else if (token == lparentsym) {
    advance();
    expression();
    if (token != rparentsym) reportParseError("expected ')' after '(' and expression in factor");
    advance();
  }
  else reportParseError("unexpected token in factor");
}


int main(int argc, string argv[]) {
  open_source_file(argv[1]);
  program();
  close_source_file();

  // from old implementation
  // printSymbolTable();
  // printCode();

  openCodeFile(argv[2]);
  saveCode();
  closeCodeFile();
  return 0;
}

