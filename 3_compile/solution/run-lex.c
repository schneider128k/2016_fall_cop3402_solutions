#include <stdio.h>
#include <string.h>
#include "tokens.h"
#include "util.h"
#include "lex.h"

extern LVAL lval;

int main(int argc, string argv[]) {
  printf("lexer for PL/0\n");
  open_source_file(argv[1]);
  token_type tok;
  while ((tok=lex()) != nulsym) {
    switch (tok) {
    case -1 :
      printf("illegal token\n");
      continue;
    case identsym : 
      printf("type: %d, value: %s\n", tok, lval.id);
      break;
    case numbersym : 
      printf("type: %d, value: %d\n", tok, lval.num);
      break;
    default :
      printf("type: %d\n", tok);
    }
  }
  close_source_file();
}
