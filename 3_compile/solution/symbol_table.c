#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "symbol_table.h"

// procedures

// print procedure

void print_proc(proc_type *proc) {
  printf("prodecure\n");
  printf("---------\n");
  printf("name : %s\n", proc->name);
  printf("level: %d\n", proc->level); 
}

proc_type *make_proc(char *name, proc_type *parent) {
  proc_type *proc = malloc(sizeof(proc_type));
  proc->name     = name;
  proc->addr     = 0;
  proc->parent   = parent;
  if (proc->parent == NULL) {
    proc->level = 0;
  }
  else {
    proc->level    = parent->level + 1;
  }
  proc->idents   = NULL;
  proc->children = NULL;
  proc->next     = NULL;
  return proc;
}

// put procedure

void put_proc(proc_type *parent, proc_type *child) {
  printf("putting child proc %s into parent proc %s\n", child->name, parent->name);

  if (parent->children == NULL) {
    parent->children = child;
    return;
  }

  proc_type *last_child;

  last_child = parent->children;
  while (last_child->next != NULL) {
    last_child = last_child->next;
  }
  last_child -> next = child;
}

// get proc

proc_type *get_proc(proc_type *parent, char *name) {

  proc_type *current_proc = parent;

  while (current_proc != NULL) {
    printf("checking children of procedure %s\n", current_proc->name);
    proc_type *current_child = current_proc->children;
    while (current_child != NULL) {
      if (strcmp(current_child->name, name) == 0) {
        return current_child;
      } 
      current_child = current_child->next;
    }
    current_proc = current_proc -> parent;
  } 
  return NULL;

}

// identifiers 

ident_type *make_constant(char *name, int num) {
  ident_type *ident = malloc(sizeof(ident_type));
  ident->kind = 1;
  ident->name = name;
  ident->num  = num;
  return ident; 
}

ident_type *make_variable(char *name, int level, int modifier) {
  ident_type *ident = malloc(sizeof(ident_type));
  ident->kind     = 2;
  ident->name     = name;
  ident->level    = level;
  ident->modifier = modifier; 
  return ident; 
}

void print_ident(ident_type *ident) {
  if (ident->kind == 1) {
    printf("  constant\n");
    printf("  --------\n");
    printf("  name     : %s\n", ident->name);
    printf("  number   : %d\n", ident->num);
  }
  else {
    printf("  variable\n");
    printf("  --------\n");
    printf("  name     : %s\n", ident->name);
    printf("  level    : %d\n", ident->level);
    printf("  modifier : %d\n", ident->modifier);
  }
}

// put identifier of a procedure into symbol table

void put_ident(proc_type *proc, ident_type *ident) {

  printf("putting identifier of procedure %s into symbol table\n", proc->name);
  print_ident(ident);

  ident_type *last_ident;

  if (proc->idents == NULL) {
    proc->idents = ident; 
    return;
  }

  last_ident = proc->idents;

  while (last_ident->next != NULL) {
    last_ident = last_ident->next;
  }

  last_ident->next = ident;
} 

// get identfier by name

ident_type *get_ident(proc_type *proc, char *name) {
  printf("getting identifier %s used inside procedure %s\n", name, proc->name);
  
  proc_type *current_proc = proc;

  while (current_proc != NULL) {
    printf("checking identifiers of procedure %s\n", current_proc->name);
    ident_type *current_ident = current_proc->idents;
    while (current_ident != NULL) {
      if (strcmp(current_ident->name, name) == 0) {
        return current_ident;
      } 
      current_ident = current_ident->next;
    }
    current_proc = current_proc -> parent;
  } 
  return NULL;
}


// print identifiers of a procedure

void print_idents(proc_type *proc) {
  printf("printing identifiers of procedure %s\n", proc->name);
  ident_type *current_ident = proc->idents;
  int num = 0;
  while (current_ident != NULL) {
    printf("printing %dth identifier\n", num++);
    print_ident(current_ident);
    current_ident = current_ident->next;
  }
}

// TO DO: tree of procedures !!!
/*
main() {
  proc_type proc;
  proc.name   = "gcd";
  proc.idents = NULL;
  proc.next   = NULL;

  ident_type ident1;
  ident1.kind  = 1;
  ident1.name  = "c";
  ident1.val   = 99;
  ident1.next  = NULL;

  ident_type ident2;
  ident2.kind = 2;
  ident2.name  = "x";
  ident2.val   = 11;
  ident2.next  = NULL;

  ident_type ident3;
  ident3.kind = 2;
  ident3.name  = "y";
  ident3.val   = 22;
  ident3.next  = NULL;

  ident_type ident4;
  ident4.kind = 2;
  ident4.name  = "z";
  ident4.val   = 33;
  ident4.next  = NULL;

  put_ident(&proc, &ident1);
  put_ident(&proc, &ident2);
  put_ident(&proc, &ident3);
  put_ident(&proc, &ident4);

  print_idents(&proc); 

  ident_type *ident = get_ident(&proc, "c");
  if (ident != NULL) {
    printf("found identifier\n");
    print_ident(ident);
  }
  else {
    printf("did not find identifier\n");
  }
}
*/
