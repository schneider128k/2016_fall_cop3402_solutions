// identifier

typedef struct ident {
  int kind;
  char *name;
  // for constants
  int num;            
  // for variables
  int level;
  int modifier;
  // next identifier
  struct ident *next; 
} ident_type;

// procedure

typedef struct proc {
  char *name;
  int level;
  int addr;
  struct ident *idents;  // identifiers
  struct proc *parent;   // parent 
  struct proc *children; // children
  struct proc *next;     // next identifier
} proc_type;

void print_ident(ident_type *ident);

void put_ident(proc_type *proc, ident_type *ident);

ident_type *get_ident(proc_type *proc, char *name);

void print_idents(proc_type *proc);

ident_type *make_constant(char *name, int num);

ident_type *make_variable(char *name, int level, int modifier);

void put_proc(proc_type *parent, proc_type *child);

proc_type *get_proc(proc_type *parent, char *name);

proc_type *make_proc(char *name, proc_type *parent);

void print_proc(proc_type *proc);




