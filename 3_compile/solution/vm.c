#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// constants
#define MAX_CODE_LENGTH 500
#define MAX_STACK_HEIGHT 2000
#define MAX_LEXI_LEVELS 3

// instruction type
typedef struct {
	int op;
	int l;
	int m;
} instruction_t;

// instruction names maybe better
const char *instruction_name[12] = {
 "   ",
 "LIT", // 01
 "OPR", // 02
 "LOD", // 03
 "STO", // 04
 "CAL", // 05
 "INC", // 06
 "JMP", // 07
 "JPC", // 08
 "SIO", // 09
};

// instruction name when opcode = 02 (opr)
const char *opr_instruction_name[14] = {
  "RET", //  0
  "NEG", //  1
  "ADD", //  2
  "SUB", //  3
  "MUL", //  4
  "DIV", //  5
  "ODD", //  6
  "MOD", //  7
  "EQL", //  8
  "NEQ", //  9
  "LSS", // 10
  "LEQ", // 11
  "GTR", // 12
  "GEQ"  // 13
};

// instruction name when opcode = 09 (SIO)
const char *sio_instruction_name[3] = {
  "OUT", // 0
  "IN ", // 1
  "HLT"  // 2
};

// code store
instruction_t code[ MAX_CODE_LENGTH ];

// stack store
int stack[ MAX_STACK_HEIGHT ];

// code length
int code_length;

// registers

// program counter
int pc;

// base pointer - points to the base of the current activation record on the stack
int bp;

// stack pointer - points to the top of the stack
int sp;

// instruction register
instruction_t ir;

// the current number of activation records
int ar_count;

// the list of positions at which the different activation records start
int ar_position_list[ MAX_LEXI_LEVELS ];
///////////////////////////////////////////////////////////////////////////////////

// pm0_code_filename
char *pm0_code_filename;

// printInstruction
void printInstruction( int line, instruction_t instruction ) {
  printf( "%3d %3d %3d %3d", line, instruction.op, instruction.m, instruction.l );
};

// printSymbolicInstruction
void printSymbolicInstruction( int line, instruction_t instruction ) {
  if (instruction.op == 2) {
    printf( "%3d  %s            ", line, opr_instruction_name[ instruction.m ] );
  }
  else if (instruction.op == 9) {
    if (instruction.m != 2) 
      printf( "%3d  %s      ", line, sio_instruction_name[ instruction.m ] );
    else 
      printf( "%3d  %s            ", line, sio_instruction_name[ instruction.m ] );
  }
  else if ((instruction.op == 3) || (instruction.op == 4) || (instruction.op == 5)) {
    printf( "%3d  %s  %3d  %3d  ", line, instruction_name[ instruction.op ], instruction.l, instruction.m );
  } 
  else {
    printf( "%3d  %s       %3d  ", line, instruction_name[ instruction.op ], instruction.m );
  }
};

// printMachineState
void printMachineState() {
  int s;
  int ar_index;

  ar_index = 1;

  printf( " %3d  %3d  %3d   ", pc, bp, sp );

  for ( s = 1; s <= sp; s++ ) {
    if ( (ar_index < ar_count ) && (s == ar_position_list[ ar_index ]) ) {
      printf( "| ");
      ar_index++;
    }
    printf( "%d ", stack[ s ] );
  }
}

// initialize
void initialize() {
  // init registers
  sp    = 0;
  bp    = 1;
  pc    = 0;
  ir.op = 0;
  ir.l  = 0;
  ir.m  = 0;

  ar_position_list[ 0 ] = 0;
  ar_count = 1;
};

// load code
void loadCode() {
  int line;
  int op, m, l;
  instruction_t instruction;

  // open this file pm0_code_filename
  FILE *fp = fopen(pm0_code_filename, "r");

  line = 0;

  for(;;) {
    if (fscanf(fp, "%d", &op) == EOF) break;
    instruction.op = op;  
    
    if (fscanf(fp, "%d", &l ) == EOF) break;
    instruction.l  = l;

    if (fscanf(fp, "%d", &m ) == EOF) break;
    instruction.m  = m;

    code[ line ] = instruction;

    printSymbolicInstruction( line, code[ line ] );
    printf( "\n" );

    line++;
  }

  code_length = line;
  fclose(fp);
}

// fetchInstruction
void fetchInstruction() {
  ir = code[ pc ];
  printSymbolicInstruction( pc, ir );
  pc++;
}

// base
int base( int level, int b ) {
  while ( level > 0 ) {
    b = stack[ b + 1 ];
    level--;
  }
  return b;
}

// executeOPR
void executeOPR() {
  switch ( ir.m ) {
    // 0 RET
    case 0 :
      ar_count--;
      //
      sp = bp - 1;
      pc = stack[ sp + 4];
      bp = stack[ sp + 3];
      break;

    // 1 NEG
    case 1 :
      stack[ sp ] = - stack[ sp ];
      break;

    // 2 ADD
    case 2 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] + stack[ sp + 1];
      break;

    // 3 SUB
    case 3 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] - stack[ sp + 1];
      break;

    // 4 MUL
    case 4 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] * stack[ sp + 1];
      break;

    // 5 DIV
    case 5 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] / stack[ sp + 1];
      break;

    // 6 ODD
    case 6 :
      stack[ sp ] = stack[ sp ] % 2;
      break;

    // 7 MOD
    case 7 :
      sp = sp - 1;
      stack[ sp ] = stack[ sp ] % stack[ sp + 1];
      break;

    // 8 EQL
    case 8 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] == stack[ sp + 1 ] );
      break;

    // 9 NEQ
    case 9 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] != stack[ sp + 1 ] );
      break;

    // 10 LSS
    case 10 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] < stack[ sp + 1 ] );
      break;

    // 11 LEQ
    case 11 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] <= stack[ sp + 1 ] );
      break;

    // 12 GTR
    case 12 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] > stack[ sp + 1 ] );
      break;

    // 13 GEQ
    case 13 :
      sp = sp - 1;
      stack[ sp ] = ( stack[ sp ] >= stack[ sp + 1 ] );
      break;
  }
}

// execute SIO
int executeSIO() {
  int input;

  switch ( ir.m ) {
    // SIO 0, 1
    // write the top stack element to the screen
    case 0 :
      printf( "\nOutput: %3d         ", stack[ sp ] );
      sp--;
      break;

    // SIO 0, 1
    // read in input from the user and store it at the top of the stack
    case 1 :
      scanf( "%d", &input );
      sp++;
      stack[ sp ] = input;
      printf( "Input:  %3d         ", stack[ sp ] );
      break;

    // SIO 0, 1
    // halt the machine
    case 2 :
      
      return 1;
  }
  return 0;
}

// executeInstruction
int executeInstruction() {
  switch ( ir.op ) {

    // LIT 0, M
    // push constant value (literal) onto stack
    case 1 :
      sp++;
      stack[ sp ] = ir.m;
      break;

    // OPR 0, M
    // execute return or arithmetic/logical operation on data on the top of stack
    case 2 :
      executeOPR();
      break;

    // LOD L, M
    // load value into top of stack from stack location at offset M from L lexicographical levels down
    case 3 :
      sp++;
      stack[ sp ] = stack[ base( ir.l, bp ) + ir.m ];
      break;

    // STO L, M
    // store value at top stack in the stack location at offset M from L lexicographical levels down
    case 4 :
      stack[ base( ir.l, bp ) + ir.m ] = stack[ sp ];
      sp--;
      break;

    // CAL L, M
    // call procedure at code index M (generates new Activation Record and pc = M)
    case 5 :
      ar_position_list[ ar_count ] = sp + 1;
      ar_count++;
      //
      stack[ sp + 1 ] = 0;                // space to return functional value (FV)
      stack[ sp + 2 ] = base( ir.l, bp ); // static link (SL)
      stack[ sp + 3 ] = bp;               // dynamic link (DL)
      stack[ sp + 4 ] = pc;               // return address
      bp = sp + 1;
      pc = ir.m;
      break;

    // INC 0, M
    // allocate M locals (increment sp by M); first four are functional value, static link, dynamic link, and return address
    case 6 :
      sp = sp + ir.m;
      break;

    // JMP 0, M
    // jump to instruction at M
    case 7 :
      pc = ir.m;
      break;

    // JPC 0, M
    // jump to instruction M if top stack element is 0
    case 8 :
      if ( stack[ sp ] == 0 ) {
        pc = ir.m;
      }
      sp--;
      break;

    // SIO 0, 1, 2
    case 9 :
      return executeSIO();
  }
  return 0;
}

// main
int main( int argc, const char* argv[]) {

  //printf("%d\n", argc);
  //printf("%s %s", argv[0], argv[1]);
  
  if (argc != 2) {
    printf("missing pl0 code filename\n");
    return 0;
  }

  pm0_code_filename = malloc(strlen(argv[1] + 1));
  strcpy(pm0_code_filename, argv[1]);

  printf("%s\n", pm0_code_filename);

  int halt;

  initialize();
  printf("PM/0 code:\n\n");
  loadCode();
  printf( "\n" );

  printf( "Execution:\n");
  printf( "                      pc   bp   sp   stack\n");
  printf( "                    ");
  printMachineState();
  printf( "\n" );

  do {
    //getchar();
    fetchInstruction();
    halt = executeInstruction();
    printMachineState();
    printf("\n");

  } while ( halt == 0 );

  return 0;
}
